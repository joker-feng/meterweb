# meterweb

#### 介绍
Meterweb是基于jmeter源码开发的b/s架构的自动化测试工具，可以集中部署，便于在团队间共享测试用例。测试数据存储于mongodb中，可随时查看历史测试数据和聚合报告，也可以根据用户来统计测试用例。

#### 软件模块

- meterweb-core：核心包
- meterweb-jmeter：jmeter源码
- meterweb-test: jmeter测试用例
- meterweb-web：web操作台


#### 项目演示地址：
[https://meterweb.fetosoft.cn/login](https://meterweb.fetosoft.cn/login "https://meterweb.fetosoft.cn/login") ，默认用户名/密码：admin/000000；

#### 使用帮助
[http://fetosoft.cn/archives/2021/08/21/339](http://fetosoft.cn/archives/2021/08/21/339)

#### 完整安装包
链接:https://pan.baidu.com/s/1dx15nc8yZUUOhi-_1EsYWg 提取码:h1pt

#### 安装说明
[http://fetosoft.cn/archives/2021/08/22/355](http://fetosoft.cn/archives/2021/08/22/355)

#### 运行效果
![输入图片说明](https://images.gitee.com/uploads/images/2021/0907/214534_6a37ce02_113373.png "meterweb.png")