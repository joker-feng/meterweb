<%@ page contentType="text/html; charset=utf-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MeterWeb - 基于JMeter的自动化测试工具</title>
    <link rel="icon" href="${ctx}/static/img/fav32.ico" type="image/x-icon"/>
	<link rel="stylesheet" type="text/css" href="${ctx}/static/tabler/css/tabler.min.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/static/tabler/css/tabler-buttons.min.css">
	<script type="text/javascript" src="${ctx}/static/easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript" src="${ctx}/static/layer/layer.js"></script>
	<style type="text/css">
		<!--
		.error {
			color: red;
		}
		-->
	</style>
	<script>
		var _hmt = _hmt || [];
		(function() {
			var hm = document.createElement("script");
			hm.src = "https://hm.baidu.com/hm.js?5d81888f92cb479f02840ccf9e8d1b64";
			var s = document.getElementsByTagName("script")[0];
			s.parentNode.insertBefore(hm, s);
		})();
	</script>
</head>

<body class="antialiased border-top-wide border-primary d-flex flex-column">
<div class="flex-fill d-flex flex-column justify-content-center">
	<div class="container-tight py-6">
		<div class="text-center mb-4">
			<img src="/static/img/logo.svg" height="80px" alt="">
		</div>
		<form id="loginForm" class="card card-md" method="post">
			<div class="card-body">
				<h2 class="mb-5 text-center">登录MeterWeb</h2>
				<div class="mb-3">
					<label class="form-label">用户名：</label>
					<input type="text" id="username" name="username" class="form-control" placeholder="请输入用户名" value="admin" autocomplete="off">
				</div>
				<div class="mb-3">
					<label class="form-label">密码：</label>
					<input type="password" id="password" name="password" class="form-control" placeholder="请输入密码" value="000000" autocomplete="off">
				</div>
				<div class="mb-2">
					<label class="form-check">
						<input type="checkbox" name="remember" class="form-check-input"/>
						<span class="form-check-label">记住登录信息</span>
					</label>
				</div>
				<div class="form-footer">
					<button type="submit" class="btn btn-primary btn-block">登录</button>
				</div>
			</div>
			<div class="hr-text">or</div>
			<div class="card-body">
				<div class="btn-list">
					<a href="https://github.com/gbinb" class="btn btn-secondary btn-block">
						<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" class="icon text-github" fill="currentColor">
							<path d="M12 .297c-6.63 0-12 5.373-12 12 0 5.303 3.438 9.8 8.205 11.385.6.113.82-.258.82-.577 0-.285-.01-1.04-.015-2.04-3.338.724-4.042-1.61-4.042-1.61C4.422 18.07 3.633 17.7 3.633 17.7c-1.087-.744.084-.729.084-.729 1.205.084 1.838 1.236 1.838 1.236 1.07 1.835 2.809 1.305 3.495.998.108-.776.417-1.305.76-1.605-2.665-.3-5.466-1.332-5.466-5.93 0-1.31.465-2.38 1.235-3.22-.135-.303-.54-1.523.105-3.176 0 0 1.005-.322 3.3 1.23.96-.267 1.98-.399 3-.405 1.02.006 2.04.138 3 .405 2.28-1.552 3.285-1.23 3.285-1.23.645 1.653.24 2.873.12 3.176.765.84 1.23 1.91 1.23 3.22 0 4.61-2.805 5.625-5.475 5.92.42.36.81 1.096.81 2.22 0 1.606-.015 2.896-.015 3.286 0 .315.21.69.825.57C20.565 22.092 24 17.592 24 12.297c0-6.627-5.373-12-12-12"/>
						</svg>
						Author's Github
					</a>
					<a href="http://fetosoft.cn/" class="btn btn-secondary btn-block">
						<svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" d="M4 5.5a.5.5 0 01.5-.5h11a.5.5 0 010 1h-11a.5.5 0 01-.5-.5zm5 3a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5zm0 3a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5zm-5 3a.5.5 0 01.5-.5h11a.5.5 0 010 1h-11a.5.5 0 01-.5-.5z" clip-rule="evenodd"></path>
							<path d="M5.734 8.352a6.586 6.586 0 00-.445.275 1.94 1.94 0 00-.346.299 1.38 1.38 0 00-.252.369c-.058.129-.1.295-.123.498h.282c.242 0 .431.06.568.182.14.117.21.29.21.521a.697.697 0 01-.187.463c-.12.14-.289.21-.503.21-.336 0-.577-.109-.721-.327-.145-.223-.217-.514-.217-.873 0-.254.055-.485.164-.692.11-.21.242-.398.399-.562.16-.168.33-.31.51-.428.179-.117.33-.213.45-.287l.211.352zm2.168 0a6.588 6.588 0 00-.445.275 1.94 1.94 0 00-.346.299c-.113.12-.199.246-.257.375a1.75 1.75 0 00-.118.492h.282c.242 0 .431.06.568.182.14.117.21.29.21.521a.697.697 0 01-.187.463c-.12.14-.289.21-.504.21-.335 0-.576-.109-.72-.327-.145-.223-.217-.514-.217-.873 0-.254.055-.485.164-.692.11-.21.242-.398.398-.562.16-.168.33-.31.51-.428.18-.117.33-.213.451-.287l.211.352z"></path>
						</svg>
						Author's Blog
					</a>
				</div>
			</div>
		</form>
		<div class="text-center text-muted">
			项目开源地址：https://gitee.com/gbinb/meterweb
		</div>
	</div>
</div>
<script type="text/javascript" src="${ctx}/static/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="${ctx}/static/js/jquery.form.min.js"></script>
<script language="JavaScript" type=text/javascript>
	$(function () {
		login();
	});

	function login() {
		$("#loginForm").validate({
			rules: {
				username: {
					required: true,
					minlength: 4
				},
				password: {
					required: true,
					minlength: 6
				}
			},
			messages: {
				username: {
					required: "请输入用户名",
					minlength: "用户名至少4位"
				},
				password: {
					required: "请输入密码",
					minlength: "密码长度6-20位"
				}
			},
			submitHandler: function() {
				let username = $('#username').val();
				let password = $('#password').val();
				let data = {username:username, password:password};
				$.post('${ctx}/userLogin', data, function (result) {
					if(result.status===1){
						location.href = "${ctx}/index";
					}else{
						layer.alert(result.errorMsg);
					}
				},'json');
			}
		});
	}
</script>
</body>
</html>