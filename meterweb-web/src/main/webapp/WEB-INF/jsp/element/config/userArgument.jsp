<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">用户定义的变量</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}', 'userArgument_form_', function(eid, status) {
                            if(status===1){
                            menuTree.updateNodeText($('#userArgument_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="userArgument_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="name" id="userArgument_form_name_${eid}" style="width:90%" data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:90%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-4">参数名称</div>
                                <div class="col-3">参数值</div>
                                <div class="col-3">描述</div>
                                <div class="col-2"><a href="javascript:void(0)" onclick="wpElement.userArgumentAdd($('#userArgument_grid_${eid}'),'','','')" class="btn btn-outline-primary">新增</a></div>
                            </div>
                            <div id="userArgument_grid_${eid}">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}', 'userArgument_form_', function(eid, status) {
                    if(status===1){
                    menuTree.updateNodeText($('#userArgument_form_name_' + eid).val());
                    }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $.get('${ctx}/auth/element/getDetail?id=${eid}', function (data) {
            if(data.status===1){
                let names = data.obj.names;
                let values = data.obj.values;
                let dests = data.obj.descriptions;
                if(names && names.length>0){
                    let container = $('#userArgument_grid_${eid}');
                    for(let i=0;i<names.length; i++){
                        wpElement.userArgumentAdd(container,names[i],values[i],dests[i]);
                    }
                }
            }
        });
    });
</script>