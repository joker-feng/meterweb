<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">JDBC数据源配置</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}', 'dataSourceConfig_form_', function(eid, status) {
                            if(status===1){
                            menuTree.updateNodeText($('#dataSourceConfig_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="dataSourceConfig_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="name" id="dataSourceConfig_form_name_${eid}" style="width:90%" data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:90%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="dbUrl" style="width:90%" data-options="label:'数据库连接URL：',labelWidth:150,required:true">
                    </div>
                    <div class="form_item_height">
                        <select class="easyui-combobox" name="driver" style="width:90%;" data-options="label:'连接驱动：',labelWidth:150,required:true">
                            <option value="com.mysql.jdbc.Driver">com.mysql.jdbc.Driver</option>
                            <option value="org.postgresql.Driver">org.postgresql.Driver</option>
                            <option value="oracle.jdbc.OracleDriver">oracle.jdbc.OracleDriver</option>
                            <option value="com.ingres.jdbc.IngresDriver">com.ingres.jdbc.IngresDriver</option>
                            <option value="com.microsoft.sqlserver.jdbc.SQLServerDriver">com.microsoft.sqlserver.jdbc.SQLServerDriver</option>
                            <option value="com.microsoft.jdbc.sqlserver.SQLServerDriver">com.microsoft.jdbc.sqlserver.SQLServerDriver</option>
                            <option value="org.apache.derby.jdbc.ClientDriver">org.apache.derby.jdbc.ClientDriver</option>
                            <option value="org.hsqldb.jdbc.JDBCDriver">org.hsqldb.jdbc.JDBCDriver</option>
                            <option value="com.ibm.db2.jcc.DB2Driver">com.ibm.db2.jcc.DB2Driver</option>
                            <option value="org.apache.derby.jdbc.ClientDriver">org.apache.derby.jdbc.ClientDriver</option>
                            <option value="org.h2.Driver">org.h2.Driver</option>
                            <option value="org.firebirdsql.jdbc.FBDriver">org.firebirdsql.jdbc.FBDriver</option>
                            <option value="org.mariadb.jdbc.Driver">org.mariadb.jdbc.Driver</option>
                            <option value="org.sqlite.JDBC">org.sqlite.JDBC</option>
                            <option value="net.sourceforge.jtds.jdbc.Driver">net.sourceforge.jtds.jdbc.Driver</option>
                            <option value="com.exasol.jdbc.EXADriver">com.exasol.jdbc.EXADriver</option>
                        </select>
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="username" style="width:90%" data-options="label:'用户名：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-passwordbox" name="password" style="width:90%" data-options="label:'密码：',labelWidth:150">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="connectionProperties" style="width:90%" data-options="label:'连接属性：',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="dataSource" style="width:90%" data-options="label:'数据源名称：',labelWidth:150,required:true">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="poolMax" style="width:90%" data-options="label:'最大连接数：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="timeout" style="width:90%" data-options="label:'连接超时（ms）：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="trimInterval" style="width:90%" data-options="label:'最大空闲时间（ms）：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <select class="easyui-combobox" name="autocommit" style="width:90%;" data-options="label:'自动提交事务：',labelWidth:150,required:true,editable:false">
                            <option value="true">True</option>
                            <option value="false">False</option>
                        </select>
                    </div>
                    <div class="form_item_height">
                        <select class="easyui-combobox" name="transactionIsolation" style="width:90%;" data-options="label:'事务隔离级别：',labelWidth:150,required:true,editable:false">
                            <option value="DEFAULT">DEFAULT</option>
                            <option value="TRANSACTION_NONE">TRANSACTION_NONE</option>
                            <option value="TRANSACTION_READ_COMMITTED">TRANSACTION_READ_COMMITTED</option>
                            <option value="TRANSACTION_READ_UNCOMMITTED">TRANSACTION_READ_UNCOMMITTED</option>
                            <option value="TRANSACTION_REPEATABLE_READ">TRANSACTION_REPEATABLE_READ</option>
                            <option value="TRANSACTION_SERIALIZABLE">TRANSACTION_SERIALIZABLE</option>
                        </select>
                    </div>
                    <div class="form_item_height">
                        <select class="easyui-combobox" name="preinit" style="width:90%;" data-options="label:'预初始化连接池：',labelWidth:150,required:true,editable:false">
                            <option value="true">True</option>
                            <option value="false">False</option>
                        </select>
                    </div>
                    <div class="form_item_height">
                        <p>
                            以换行分隔多条sql语句
                        </p>
                        <div>
                            <input class="easyui-textbox" name="initQuery" style="width:90%"
                                   data-options="label:'预初始化sql语句：',labelWidth:150,multiline:true,height:150">
                        </div>
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <select class="easyui-combobox" name="keepAlive" style="width:90%;" data-options="label:'测试连接池：',labelWidth:150,required:true,editable:false">
                            <option value="true">True</option>
                            <option value="false">False</option>
                        </select>
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="connectionAge" style="width:90%" data-options="label:'测试空闲时间（ms）：',labelWidth:150">
                    </div>
                    <div>
                        <select class="easyui-combobox" name="checkQuery" style="width:90%;" data-options="label:'测试SQL：',labelWidth:150">
                            <option value="select 1">select 1</option>
                            <option value="select 1 from INFORMATION_SCHEMA.SYSTEM_USERS">select 1 from INFORMATION_SCHEMA.SYSTEM_USERS</option>
                            <option value="select 1 from dual">select 1 from dual</option>
                            <option value="select 1 from sysibm.sysdummy1">select 1 from sysibm.sysdummy1</option>
                            <option value="select 1 from rdb$database">select 1 from rdb$database</option>
                        </select>
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}', 'dataSourceConfig_form_', function(eid, status) {
                    if(status===1){
                    menuTree.updateNodeText($('#dataSourceConfig_form_name_' + eid).val());
                    }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>
