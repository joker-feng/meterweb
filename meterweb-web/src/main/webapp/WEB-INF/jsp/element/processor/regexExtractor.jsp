<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">正则表达式提取器</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','regexExtractor_form_',function(eid,status) {
                            if(status===1){
                            menuTree.updateNodeText($('#regexExtractor_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="regexExtractor_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" id="regexExtractor_form_name_${eid}" name="name" style="width:100%"
                               data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:100%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <p>应用范围：</p>
                    <div class="form_item_height">
                        <div class="row">
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="scope" value="all" data-options="label:'主样本及子样本：',labelWidth:150">
                            </div>
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="scope" value="parent" data-options="label:'仅主样本：',labelWidth:100">
                            </div>
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="scope" value="children" data-options="label:'仅子样本：',labelWidth:100">
                            </div>
                            <div class="col-3"></div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="scope" value="variable" data-options="label:'自定义的变量名：',labelWidth:150">
                            </div>
                            <div class="col-9">
                                <input class="easyui-textbox" name="scopeVariable" style="width:100%" data-options="labelWidth:100">
                            </div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <p>要检查的响应字段：</p>
                    <div class="form_item_height">
                        <div class="row">
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="useField" value="false" data-options="label:'Body',labelWidth:150,labelPosition:'after'">
                            </div>
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="useField" value="unescaped" data-options="label:'Body (unescaped)',labelWidth:150,labelPosition:'after'">
                            </div>
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="useField" value="as_document" data-options="label:'Body as a Document',labelWidth:150,labelPosition:'after'">
                            </div>
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="useField" value="true" data-options="label:'Response Headers',labelWidth:150,labelPosition:'after'">
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="useField" value="request_headers" data-options="label:'Request Headers',labelWidth:150,labelPosition:'after'">
                            </div>
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="useField" value="URL" data-options="label:'URL',labelWidth:150,labelPosition:'after'">
                            </div>
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="useField" value="code" data-options="label:'Response Code',labelWidth:150,labelPosition:'after'">
                            </div>
                            <div class="col-3">
                                <input class="easyui-radiobutton" name="useField" value="message" data-options="label:'Response Message',labelWidth:150,labelPosition:'after'">
                            </div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="refname" style="width:100%" data-options="label:'引用名称：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="regex" style="width:100%" data-options="label:'正则表达式：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="template" style="width:100%" data-options="label:'模板：',labelWidth:150">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="matchNumber" style="width:100%" data-options="label:'匹配数字(0随机)：',labelWidth:150">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="defaultValue" style="width:60%" data-options="label:'缺省值：',labelWidth:150">
                        <input class="easyui-checkbox" name="defaultEmptyValue" value="true" data-options="label:'使用空默认值',labelWidth:150,labelPosition:'after'">
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','regexExtractor_form_',function(eid,status) {
                    if(status===1){
                    menuTree.updateNodeText($('#regexExtractor_form_name_' + eid).val());
                    }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>