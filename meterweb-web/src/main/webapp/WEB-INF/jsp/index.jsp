<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>MeterWeb - 基于JMeter的自动化测试工具</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/static/easyui-1.7.0/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/easyui-1.7.0/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/easyui-1.7.0/themes/color.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/json-viewer/jquery.json-viewer.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/tabler/css/tabler.min.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/tabler/css/tabler-buttons.min.css">
    <script type="text/javascript" src="${ctx}/static/easyui-1.7.0/jquery.min.js"></script>
    <script type="text/javascript" src="${ctx}/static/easyui-1.7.0/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx}/static/easyui-1.7.0/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/static/easyui-1.7.0/easyui.validate.js"></script>
    <script type="text/javascript" src="${ctx}/static/js/jsextends.js"></script>
    <script type="text/javascript" src="${ctx}/static/json-viewer/jquery.json-viewer.js"></script>
    <script type="text/javascript" src="${ctx}/static/layer/layer.js"></script>
    <script type="text/javascript" src="${ctx}/static/js/menu.js?v=210911"></script>
    <style type="text/css">
        .form_item_height{
            margin-bottom: 25px;
        }
        .btn-ghost-primary {
            color: #000000; !important;
            padding: .25rem .6rem; !important;
            background-color: transparent;
            border-color: transparent;
        }
    </style>
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?5d81888f92cb479f02840ccf9e8d1b64";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
</head>

<body>
<div class="easyui-layout" data-options="fit:true" >
    <div data-options="region:'north',border:false" style="height:80px; overflow-x:hidden;">
        <div class="easyui-panel" data-options="border:false" style="padding:8px 10px; height: 72px;">
            <span style="font-size: 24px; padding-top: 10px; padding-right: 30px;">MeterWeb自动化测试工具</span>
            <a href="javascript:void(0)" onclick="homePage()" class="btn btn-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M9.646 3.146a.5.5 0 01.708 0l6 6a.5.5 0 01.146.354v7a.5.5 0 01-.5.5h-4.5a.5.5 0 01-.5-.5v-4H9v4a.5.5 0 01-.5.5H4a.5.5 0 01-.5-.5v-7a.5.5 0 01.146-.354l6-6zM4.5 9.707V16H8v-4a.5.5 0 01.5-.5h3a.5.5 0 01.5.5v4h3.5V9.707l-5.5-5.5-5.5 5.5z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M15 4.5V8l-2-2V4.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"></path>
                </svg>
                首页
            </a>
            <a href="javascript:void(0)" onclick="addTestPlan()" class="btn btn-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M10 5.5a.5.5 0 01.5.5v4a.5.5 0 01-.5.5H6a.5.5 0 010-1h3.5V6a.5.5 0 01.5-.5z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M9.5 10a.5.5 0 01.5-.5h4a.5.5 0 010 1h-3.5V14a.5.5 0 01-1 0v-4z" clip-rule="evenodd"></path>
                </svg>
                新建测试计划
            </a>
            <a id="link_startTest" href="javascript:void(0)" onclick="startTest()" class="btn btn-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.596 10.697l-6.363 3.692c-.54.313-1.233-.066-1.233-.697V6.308c0-.63.692-1.01 1.233-.696l6.363 3.692a.802.802 0 010 1.393z"></path>
                </svg>
                启动线程组
            </a>
            <a id="link_sampler" href="javascript:void(0)" onclick="addSamplerPanel()" class="btn btn-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M13.293 3.293a1 1 0 011.414 0l2 2a1 1 0 010 1.414l-9 9a1 1 0 01-.39.242l-3 1a1 1 0 01-1.266-1.265l1-3a1 1 0 01.242-.391l9-9zM14 4l2 2-9 9-3 1 1-3 9-9z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M14.146 8.354l-2.5-2.5.708-.708 2.5 2.5-.708.708zM5 12v.5a.5.5 0 00.5.5H6v.5a.5.5 0 00.5.5H7v.5a.5.5 0 00.5.5H8v-1.5a.5.5 0 00-.5-.5H7v-.5a.5.5 0 00-.5-.5H5z" clip-rule="evenodd"></path>
                </svg>
                取样数据
            </a>
            <a id="link_counter" href="javascript:void(0)" onclick="addAggregatePanel()" class="btn btn-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M10 17a7 7 0 100-14 7 7 0 000 14zm0 1a8 8 0 100-16 8 8 0 000 16z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M9.5 9.793V3h1v6.5H17v1h-6.793l-4.853 4.854-.708-.708L9.5 9.793z" clip-rule="evenodd"></path>
                </svg>
                聚合数据
            </a>
            <a id="link_timer" href="javascript:void(0)" onclick="addScheduledPanel()" class="btn btn-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M10 17a6 6 0 100-12 6 6 0 000 12zm0 1a7 7 0 100-14 7 7 0 000 14z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M10 6.5a.5.5 0 01.5.5v4a.5.5 0 01-.5.5H6.5a.5.5 0 010-1h3V7a.5.5 0 01.5-.5zm-2.5-4A.5.5 0 018 2h4a.5.5 0 010 1H8a.5.5 0 01-.5-.5z" clip-rule="evenodd"></path>
                    <path d="M9 3h2v2H9V3z"></path>
                </svg>
                定时测试
            </a>
            <a id="link_userManager" href="javascript:void(0)" onclick="addUserPanel()" class="btn btn-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M15 16s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002zM5.022 15h9.956a.274.274 0 00.014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C13.516 12.68 12.289 12 10 12c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 00.022.004zm9.974.056v-.002zM10 9a2 2 0 100-4 2 2 0 000 4zm3-2a3 3 0 11-6 0 3 3 0 016 0z" clip-rule="evenodd"></path>
                </svg>
                用户管理
            </a>
            <a href="javascript:void(0)" onclick="logout()" class="btn btn-primary">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M6.354 13.354a.5.5 0 000-.708L3.707 10l2.647-2.646a.5.5 0 10-.708-.708l-3 3a.5.5 0 000 .708l3 3a.5.5 0 00.708 0z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M13.5 10a.5.5 0 00-.5-.5H4a.5.5 0 000 1h9a.5.5 0 00.5-.5z" clip-rule="evenodd"></path>
                    <path fill-rule="evenodd" d="M16 15.5a1.5 1.5 0 001.5-1.5V6A1.5 1.5 0 0016 4.5H9A1.5 1.5 0 007.5 6v1.5a.5.5 0 001 0V6a.5.5 0 01.5-.5h7a.5.5 0 01.5.5v8a.5.5 0 01-.5.5H9a.5.5 0 01-.5-.5v-1.5a.5.5 0 00-1 0V14A1.5 1.5 0 009 15.5h7z" clip-rule="evenodd"></path>
                </svg>
                退出
            </a>
            <a href="http://fetosoft.cn/archives/2021/08/21/339" target="_blank" class="btn btn-orange">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533l1.002-4.705zM10 7.5a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"></path>
                </svg>
                帮助文档
            </a>
            <a href="http://fetosoft.cn/archives/2021/09/07/429" target="_blank" class="btn btn-orange">
                <svg class="icon icon-md" width="1em" height="1em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M4 3a1 1 0 00-1 1v4.586a1 1 0 00.293.707l7 7a1 1 0 001.414 0l4.586-4.586a1 1 0 000-1.414l-7-7A1 1 0 008.586 3H4zm4 3.5a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0z" clip-rule="evenodd"></path>
                </svg>
                更新日志
            </a>
        </div>
        <div class="row" style="background-color: #6383a8; height: 8px;">
        </div>
    </div>
    <div data-options="region:'center',iconCls:'icon-ok',split:false,border:false">
        <div id="main_tabs" class="easyui-tabs" data-options="fit:true,showHeader:false">
            <div title="测试计划" data-options="iconCls:'icon-home'">
                <div class="easyui-layout" data-options="fit:true" >
                    <div data-options="region:'west',split:true,border:false" style="width:260px;">
                        <div class="easyui-panel" style="padding:5px; border:0">
                            <ul id="navigate_tree" class="easyui-tree" data-options="animate:true,dnd:true"></ul>
                        </div>
                    </div>
                    <div data-options="region:'center',iconCls:'icon-ok',split:false,border:false">
                        <div id="testPlan_tabs" class="easyui-tabs" data-options="fit:true,plain:true,narrow:true,border:false">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div data-options="region:'south'" style="height:3px;"></div>
</div>
<div id="tab_menu" class="easyui-menu" style="width:160px;">
    <div onclick="testPlanTab.refreshTab()" data-options="iconCls:'icon-reload'">刷新</div>
    <div onclick="testPlanTab.closeCurrentTab()" data-options="iconCls:'icon-no'">关闭当前</div>
    <div onclick="testPlanTab.closeAllTab()" data-options="iconCls:'icon-no'">关闭所有</div>
</div>
<div id="tree_menu" class="easyui-menu" style="width:200px;"></div>
<jsp:include page="user/dialog.jsp" />
<jsp:include page="scheduled/dialog.jsp" />
<script type="text/javascript" src="${ctx}/static/js/woodpecker.js?v=210902"></script>
<script type="text/javascript" src="${ctx}/static/js/wp_element.js"></script>
<script type="text/javascript" src="${ctx}/static/js/wp_system.js"></script>
<script type="text/javascript" src="${ctx}/static/js/wp_scheduled.js"></script>
<script type="text/javascript">
    contextPath = '${ctx}';
    let global_planId = '', global_testId = '';
    let menuTree = new MenuTree(contextPath, 'navigate_tree');
    let testPlanTab = new Tab(contextPath, 'testPlan_tabs');
    let mainTab = new Tab(contextPath, 'main_tabs');
    let planNotice = new TestResultNotice('/auth/plan/notice');

    let samplerTree = new SamplerTree(contextPath);
    let historySamplerTree = new SamplerTree(contextPath);
    let samplerNotice = new TestResultNotice();

    let testElement = new TestElement(contextPath);
    let report = new Report(contextPath);
    let wpElement = new WpElement(contextPath);
    $(function(){
        menuTree.tab = $('#main_tabs');
        menuTree.buildTree();
        menuTree.contextMenu = $('#tree_menu');
        menuTree.clickEvent = function(node){
            node.url = '${ctx}/auth/element/gotoPage?page=' + node.category + '&id=' + node.id;
            addPanel(menuTree.tree, node);
        };

        testPlanTab.contextMenu = $('#tab_menu');
        testPlanTab.selectEvent = function(tabId){
            let navTree = menuTree.getTree();
            let index = tabId.lastIndexOf('_');
            let nodeId = tabId.substr(index + 1);
            let node = navTree.tree('find', nodeId);
            if(node && node.target){
                navTree.tree('select', node.target);
                navTree.tree('expandTo', node.target);
            }
        };

        testPlanTab.loadPanelEvent = function (panel) {
            let pid = panel.panel("options").id;
            if(!(pid.indexOf('system')===0)){
                let index = pid.lastIndexOf('_');
                let eid = pid.substr(index + 1);
                loadElementFormData(eid);
            }
        };

        connectSocket();
    });

    function connectSocket() {
        planNotice.messageEvent = function (data) {
            if(data.type===2){
                global_planId = data.data.planId;
                global_testId = data.data.testId;
                $('#link_startTest').removeClass('disabled');
                if(data.data.success){
                    layerSlide('测试结束');
                }else{
                    layerSlide(data.data.errorMsg);
                }
            }
        };
        planNotice.connect();
    }

    function homePage() {
        let tabs = $('#main_tabs');
        let tab = tabs.tabs('getSelected');
        let tabIndex = tabs.tabs('getTabIndex', tab);
        if(tabIndex>0){
            tabs.tabs('select', 0);
        }
    }

    function addTestPlan() {
        homePage();
        let planData = {
            category: 'testPlan',
            text: '测试计划',
            parentId: '0000'
        };
        postJson('${ctx}/auth/element/create', planData, function (success, result) {
            if(success && result.status===1){
                let element = result.obj;
                let node = menuTree.createChildNode(element);
                node.url = '${ctx}/auth/element/gotoPage?page=' + element.category + '&id=' + element.id;
                menuTree.tree.tree('append', {
                    data: [node]
                });
                addPanel(menuTree.tree, node);
            }
        },'json');
    }

    function addPanel(tree, node){
        if(node.url===undefined || node.url===''){
            if(node.children.length>0){
                tree.tree("toggle", node.target);
            }
        }else{
            testPlanTab.addPanel(node);
        }
    }

    function loadElementFormData(eid) {
        $.get('${ctx}/auth/element/getDetail?id=' + eid, function (data) {
            if(data.status===1){
                let formId = data.obj.category + "_form_" + eid;
                $('#' + formId).form('load', data.obj);
            }
        });
    }

    function startTest() {
        let node = menuTree.tree.tree("getSelected");
        if(node===undefined || node===null || node.planId===''){
            layer.alert('请选择测试项目!');
            return;
        }
        let data = {"planId":node.planId};
        let testBtn = $('#link_startTest');
        testBtn.addClass('disabled');
        postJson('${ctx}/auth/element/startTest', data, function (success, result) {
            if(success){
                if(result.status===1){
                    layerSlide('启动成功!');
                }else{
                    layer.alert(result.errorMsg);
                    testBtn.removeClass('disabled');
                }
            }else{
                layer.alert(result);
                testBtn.removeClass('disabled');
            }
        });
    }

    /**
     * 用户管理面板
     */
    function addUserPanel() {
        let node = {
            id: 'user',
            category: 'system',
            text: '用户管理',
            url: '${ctx}/auth/user/main'
        };
        mainTab.addPanel(node);
    }

    /**
     * 定时任务
     */
    function addScheduledPanel() {
        let node = {
            id: 'scheduled',
            category: 'system',
            text: '定时测试任务',
            url: '${ctx}/auth/scheduled/main'
        };
        mainTab.addPanel(node);
    }

    /**
     * 取样数据
     */
    function addSamplerPanel() {
        let node = {
            id: 'sampler',
            category: 'system',
            text: '测试取样数据',
            url: '${ctx}/auth/report/sampler'
        };
        mainTab.addPanel(node);
    }

    /**
     * 报表记录
     */
    function addAggregatePanel() {
        let node = {
            id: 'aggregate',
            category: 'system',
            text: '聚合报表数据',
            url: '${ctx}/auth/report/aggregate'
        };
        mainTab.addPanel(node);
    }

    /**
     * 退出系统
     */
    function logout() {
        let ly = layerConfirm('确定要退出吗？', function (r) {
            if(r){
                $.post('${ctx}/logout', function (result) {
                    if(result.status===1){
                        location.href = '${ctx}/login';
                    }else{
                        layer.close(ly);
                        layer.alert(result.errorMsg);
                    }
                },'json');
            }
        });
    }
</script>
</body>
</html>