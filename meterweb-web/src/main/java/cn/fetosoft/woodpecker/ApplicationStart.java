package cn.fetosoft.woodpecker;

import cn.fetosoft.commons.utils.NetUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.core.env.Environment;

/**
 * 启动类
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/7 14:39
 */
@ServletComponentScan
@SpringBootApplication(scanBasePackages = {"cn.fetosoft.woodpecker", "cn.fetosoft.commons.scheduled"})
public class ApplicationStart {

	private static Environment env;

	@Autowired
	public void setEnvironment(Environment environment){
		env = environment;
	}

	public static void main(String[] args) {
		SpringApplication.run(ApplicationStart.class, args);
		System.out.println("The server is start!!!\n");
		String host = NetUtil.getSingleLocalIP();
		String port = env.getProperty("local.server.port");
		String contextPath = env.getProperty("server.servlet.context-path");
		System.out.println("\n\nhttp://" + host + ":" + port + contextPath + "\n\n");
	}
}
