package cn.fetosoft.woodpecker.enums;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/6 20:42
 */
public enum MessageType {

	/**
	 * 心跳
	 */
	Heart(0, "心跳信息"),

    /**
     * 连接成功
     */
    ConnSuccess(1, "WebSocket连接成功"),

	/**
	 * 测试计划执行结束
	 */
    TestComplete(2, "测试完成");

    private int name;
    private String text;

    MessageType(int name, String text){
        this.name = name;
        this.text = text;
    }

    public int getName(){
        return this.name;
    }

    public String getText(){
        return this.text;
    }
}
