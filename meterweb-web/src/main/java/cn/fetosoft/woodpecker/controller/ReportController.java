package cn.fetosoft.woodpecker.controller;

import cn.fetosoft.woodpecker.core.data.entity.Report;
import cn.fetosoft.woodpecker.core.data.entity.TestResult;
import cn.fetosoft.woodpecker.core.data.form.ReportForm;
import cn.fetosoft.woodpecker.core.data.form.TestResultForm;
import cn.fetosoft.woodpecker.core.data.service.ReportService;
import cn.fetosoft.woodpecker.core.data.service.TestResultService;
import cn.fetosoft.woodpecker.core.enums.DateFormatEnum;
import cn.fetosoft.woodpecker.struct.DataGrid;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 报表
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/7 9:45
 */
@Slf4j
@Controller
@RequestMapping("/auth/report")
public class ReportController {

	@Autowired
	private ReportService reportService;
	@Autowired
	private TestResultService testResultService;

	/**
	 * 采样数据页面
	 * @return
	 */
	@RequestMapping("/sampler")
	public String samplerPage(){
		return "/reports/sampler";
	}

	/**
	 * 报表页面
	 * @return
	 */
	@RequestMapping("/aggregate")
	public String aggregatePage(){
		return "/reports/aggregate";
	}

	/**
	 * 查询聚合报表
	 * @param form
	 * @return
	 */
	@RequestMapping("/getAggregateList")
	@ResponseBody
	public String getAggReportList(@ModelAttribute ReportForm form){
		DataGrid<Report> dataGrid = new DataGrid<>();
		form.setAscField("createTime");
		try {
			if(StringUtils.isNotBlank(form.getTestDate())){
				form.setStartTime(DateFormatEnum.YMD_HMS.stringToDate(form.getTestDate() + " 00:00:00"));
				form.setEndTime(DateFormatEnum.YMD_HMS.stringToDate(form.getTestDate() + " 23:59:59"));
			}
			long total = reportService.selectCountByForm(form, Report.class);
			dataGrid.setTotal(total);
			if(total>0){
				List<Report> list = reportService.selectListByForm(form, Report.class);
				dataGrid.setRows(list);
			}
		} catch (Exception e) {
			log.error("getAggregateList", e);
		}
		return JSON.toJSONString(dataGrid);
	}

	/**
	 * 删除报表数据
	 * @param ids
	 * @return
	 */
	@PostMapping("/deleteAggregate")
	@ResponseBody
	public String deleteAggregate(@RequestParam String ids){
		Result<String> result = Result.errorResult();
		String[] arr = ids.split(",");
		try{
			long count = 0;
			for(String id : arr){
				count += reportService.deleteAggregateById(id);
			}
			if(count>0){
				result.setStatus(Result.SUCCESS);
			}
		}catch(Exception e){
			result.setErrorMsg("Delete data exception!!!");
			log.error("deleteAggregate", e);
		}
		return result.toString();
	}

	/**
	 * 查询聚合报表
	 * @param form
	 * @return
	 */
	@RequestMapping("/getSamplerList")
	@ResponseBody
	public String getSamplerList(@ModelAttribute TestResultForm form){
		DataGrid<TestResult> dataGrid = new DataGrid<>();
		try {
			form.setDescField("createTime");
			if(StringUtils.isNotBlank(form.getTestDate())){
				form.setStartTime(DateFormatEnum.YMD_HMS.stringToDate(form.getTestDate() + " 00:00:00"));
				form.setEndTime(DateFormatEnum.YMD_HMS.stringToDate(form.getTestDate() + " 23:59:59"));
			}
			long total = testResultService.selectCountByForm(form, TestResult.class);
			dataGrid.setTotal(total);
			if(total>0){
				List<TestResult> list = testResultService.selectListByForm(form, TestResult.class);
				dataGrid.setRows(list);
			}
		} catch (Exception e) {
			log.error("getSamplerList", e);
		}
		return JSON.toJSONString(dataGrid);
	}

	/**
	 * 删除取样数据
	 * @param ids
	 * @return
	 */
	@PostMapping("/deleteSampler")
	@ResponseBody
	public String deleteSampler(@RequestParam String ids){
		Result<String> result = Result.errorResult();
		String[] arr = ids.split(",");
		try{
			long count = 0;
			for(String id : arr){
				count += testResultService.deleteById(id, TestResult.class).getDeletedCount();
			}
			if(count>0){
				result.setStatus(Result.SUCCESS);
			}
		}catch(Exception e){
			result.setErrorMsg("Delete data exception!!!");
			log.error("deleteSampler", e);
		}
		return result.toString();
	}
}
