package cn.fetosoft.woodpecker.controller;

import cn.fetosoft.commons.secret.HmacSHA1Signer;
import cn.fetosoft.commons.utils.NetUtil;
import cn.fetosoft.woodpecker.config.Constant;
import cn.fetosoft.woodpecker.core.data.entity.User;
import cn.fetosoft.woodpecker.core.data.service.UserService;
import cn.fetosoft.woodpecker.struct.UserIdentity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.StringJoiner;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/16 14:10
 */
@Slf4j
@Controller
public class MainController extends BaseController {

	@Autowired
	private UserService userService;

	/**
	 * 跳转到登录页
	 * @Author guobingbing
	 * @Date 2016/12/8 16:35
	 * @Param
	 * @Return
	 */
	@RequestMapping(value = {"/", "/login", "/index", "/main"})
	public String main(HttpServletRequest request){
		UserIdentity user = this.getSessionUser(request);
		if(user!=null){
			return "/index";
		}else{
			return "/login";
		}
	}

	/**
	 * 帮助页
	 * @return
	 */
	@RequestMapping("/help")
	public String help(){
		return "/help";
	}

	/**
	 * 登录
	 */
	@RequestMapping("/userLogin")
	@ResponseBody
	public String userLogin(String username, String password, String remember,
							HttpServletRequest request){
		Result<String> result = Result.errorResult();
		User user = userService.login(username, HmacSHA1Signer.signString(password, Constant.HMAC_SHA1_KEY));
		if(user!=null){
			User loginUser = new User();
			loginUser.setId(user.getId());
			loginUser.setLoginIp(NetUtil.getRemoteIp(request));
			loginUser.setLoginTime(System.currentTimeMillis());
			try {
				userService.update(loginUser);
			} catch (Exception e) {
				log.error("userLogin", e);
			}
			UserIdentity identity = new UserIdentity(user.getUserId(), user.getUsername());
			identity.setName(user.getName());
			identity.setMail(user.getMail());
			identity.setAvatar(user.getAvatar());
			this.setSessionUser(identity, request);
			result.setStatus(Result.SUCCESS);
		}else{
			result.setErrorMsg("用户名或密码错误！");
		}
		return result.toString();
	}

	/**
	 * 获取登录用户的ID
	 * @param request
	 * @return
	 */
	@RequestMapping("/getUserId")
	@ResponseBody
	public String getUserId(HttpServletRequest request){
		Result<String> result = Result.errorResult();
		UserIdentity identity = this.getSessionUser(request);
		if(identity!=null){
			result.setObj(identity.getUserId());
			result.setStatus(Result.SUCCESS);
		}
		return result.toString();
	}

	/**
	 * 登出
	 * @return
	 */
	@RequestMapping("/logout")
	@ResponseBody
	public String logout(HttpServletRequest request){
		this.clearSessionUser(request);
		return Result.successResult().toString();
	}

	/**
	 * form表单提交测试
	 * @param request
	 * @return
	 */
	@RequestMapping("/acceptForm")
	@ResponseBody
	public String acceptForm(HttpServletRequest request){
		StringJoiner joiner = new StringJoiner("&");
		Enumeration<String> iter = request.getParameterNames();
		while (iter.hasMoreElements()){
			String name = iter.nextElement();
			String value = request.getParameter(name);
			joiner.add(name + "=" + value);
		}
		return joiner.toString();
	}

	/**
	 * post json
	 * @param request
	 * @return
	 */
	@RequestMapping("/acceptBody")
	@ResponseBody
	public String acceptBody(HttpServletRequest request){
		try{
			return IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
		}catch(Exception e){
			return e.getMessage();
		}
	}
}
