package cn.fetosoft.woodpecker.config;

import cn.fetosoft.commons.secret.HmacSHA1Signer;
import cn.fetosoft.woodpecker.core.data.entity.User;
import cn.fetosoft.woodpecker.core.data.form.UserForm;
import cn.fetosoft.woodpecker.core.data.service.UserService;
import cn.fetosoft.woodpecker.core.util.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 初始化基础数据
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/8/5 20:52
 */
@Slf4j
@Component
public class InitializeBasicData implements CommandLineRunner {

    @Autowired
    private UserService userService;

    @Override
    public void run(String... args) throws Exception {
        this.initAdmin();
    }

    private void initAdmin(){
        try{
            long total = userService.selectCountByForm(new UserForm(), User.class);
            if(total<1){
                User user = new User();
                user.setUserId("U" + RandomUtil.getLenRandom(8));
                user.setUsername("admin");
                user.setPassword(HmacSHA1Signer.signString("000000", Constant.HMAC_SHA1_KEY));
                user.setName("管理员");
                user.setNote("系统保留账户");
                user.setMail("gbinb@126.com");
                user.setCreateTime(System.currentTimeMillis());
                userService.insert(user);
            }
        }catch (Exception e){
            log.error("initUser", e);
        }
    }
}
