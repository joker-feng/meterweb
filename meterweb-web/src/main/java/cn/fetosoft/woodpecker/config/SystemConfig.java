package cn.fetosoft.woodpecker.config;

import lombok.Getter;
import lombok.Setter;
import org.apache.catalina.Context;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/8 15:00
 */
@Setter
@Getter
@Configuration
public class SystemConfig {

	@Bean
	public TomcatServletWebServerFactory createTomcatFactory(){
		return new TomcatServletWebServerFactory(){
			@Override
			protected void postProcessContext(Context context) {
				((StandardJarScanner)context.getJarScanner()).setScanManifest(false);
			}
		};
	}

	@Bean
	public ServerEndpointExporter serverEndpointExporter() {
		return new ServerEndpointExporter();
	}
}
