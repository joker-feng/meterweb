package cn.fetosoft.woodpecker;

import cn.fetosoft.commons.secret.HmacSHA1Signer;
import cn.fetosoft.woodpecker.config.Constant;
import cn.fetosoft.woodpecker.core.data.entity.User;
import cn.fetosoft.woodpecker.core.data.service.UserService;
import cn.fetosoft.woodpecker.core.util.RandomUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/18 9:36
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ApplicationStart.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserTest {

    @Autowired
    private UserService userService;

    @Test
    public void insert(){
        User user = new User();
        user.setUserId("U" + RandomUtil.getLenRandom(8));
        user.setUsername("admin");
        user.setPassword(HmacSHA1Signer.signString("000000", Constant.HMAC_SHA1_KEY));
        user.setName("管理员");
        user.setNote("系统保留账户");
        user.setMail("gbinb@126.com");
        user.setCreateTime(System.currentTimeMillis());
        try {
            userService.insert(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
