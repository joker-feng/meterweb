package cn.fetosoft.woodpecker.core.data.entity.element.processor;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/29 14:38
 */
@Setter
@Getter
@Document("wp_element")
public class AesSecretElement extends BaseElement {

	@DataField
	private String cipherMode;

	@DataField
	private String fillMode;

	@DataField
	private String ivOffset;

	@DataField
	private String hexKey;

	@DataField
	private String encoding = "utf-8";

	@DataField
	private String inputParams;

	@DataField
	private String outputParams;

	@DataField
	private String encOrDec;

	@DataField
	private String usedPrevResult;
}
