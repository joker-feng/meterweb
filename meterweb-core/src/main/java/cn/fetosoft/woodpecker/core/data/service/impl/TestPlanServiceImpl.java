package cn.fetosoft.woodpecker.core.data.service.impl;

import cn.fetosoft.woodpecker.core.data.base.AbstractMongoService;
import cn.fetosoft.woodpecker.core.data.entity.element.TestPlanElement;
import cn.fetosoft.woodpecker.core.data.form.TestPlanForm;
import cn.fetosoft.woodpecker.core.data.service.TestPlanService;
import org.springframework.stereotype.Service;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/8 10:02
 */
@Service
public class TestPlanServiceImpl extends AbstractMongoService<TestPlanElement, TestPlanForm>
		implements TestPlanService {

}
