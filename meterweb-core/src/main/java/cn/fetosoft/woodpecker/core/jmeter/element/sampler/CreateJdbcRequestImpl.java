package cn.fetosoft.woodpecker.core.jmeter.element.sampler;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.sampler.JdbcRequestElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.jmeter.protocol.jdbc.sampler.JDBCSampler;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建JDBCSampler
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/9 17:14
 */
@Component("jdbcRequestImpl")
public class CreateJdbcRequestImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		JdbcRequestElement element = (JdbcRequestElement) be;
		JDBCSampler sampler = new JDBCSampler();
		sampler.setProperty("dataSource", element.getDataSource());
		sampler.setProperty("queryType", element.getQueryType());
		sampler.setProperty("query", element.getQuery());
		sampler.setProperty("queryArguments", element.getQueryArguments());
		sampler.setProperty("queryArgumentsTypes", element.getQueryArgumentsTypes());
		sampler.setProperty("variableNames", element.getVariableNames());
		sampler.setProperty("resultVariable", element.getResultVariable());
		sampler.setProperty("queryTimeout", element.getQueryTimeout());
		sampler.setProperty("resultSetMaxRows", element.getResultSetMaxRows());
		sampler.setProperty("resultSetHandler", element.getResultSetHandler());
		return sampler;
	}
}
