package cn.fetosoft.woodpecker.core.data.form;

import cn.fetosoft.woodpecker.core.data.base.Condition;
import cn.fetosoft.woodpecker.core.data.base.ConditionType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.util.Date;

/**
 * 报表查询器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/1 16:06
 */
@Setter
@Getter
@Document("wp_report")
public class ReportForm extends ElementForm {

    @Condition
    private String testId;

    private String testDate;

    @Condition(fieldType = FieldType.DATE_TIME, fieldName = "createTime", conditionType = ConditionType.GTE)
    private Date startTime;

    @Condition(fieldType = FieldType.DATE_TIME, fieldName = "createTime", conditionType = ConditionType.LTE)
    private Date endTime;
}
