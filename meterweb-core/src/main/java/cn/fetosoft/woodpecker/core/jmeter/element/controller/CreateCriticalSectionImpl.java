package cn.fetosoft.woodpecker.core.jmeter.element.controller;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.controller.CriticalSectionElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.jmeter.control.CriticalSectionController;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建临界部分控制器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/11 20:58
 */
@Component("criticalSectionImpl")
public class CreateCriticalSectionImpl extends AbstractElementBuild {

    @Override
    protected TestElement convert(BaseElement be) throws Exception {
        CriticalSectionElement element = (CriticalSectionElement) be;
        CriticalSectionController controller = new CriticalSectionController();
        controller.setLockName(element.getLockName());
        return controller;
    }
}
