package cn.fetosoft.woodpecker.core.data.entity.element.config;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * ID生成器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/21 17:54
 */
@Setter
@Getter
@Document("wp_element")
public class IdGeneratorElement extends BaseElement {

	/**
	 * 类型：uuid,date,number
	 */
	@DataField
	private String mode;

	@DataField
	private String paramNames;

	@DataField
	private Integer length;

	@DataField
	private String pattern;

	@DataField
	private String prefix;
}
