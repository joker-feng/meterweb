package cn.fetosoft.woodpecker.core.data.entity.element.sampler;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 脚本取样器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/9/5 10:34
 */
@Setter
@Getter
@Document("wp_element")
public class BeanShellSamplerElement extends BaseElement {

    @DataField
    private String script;

    @DataField
    private String resetInterpreter;

    @DataField
    private String parameters = "";

    @DataField
    private String filename = "";
}
