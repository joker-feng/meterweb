package cn.fetosoft.woodpecker.core.data.entity.element.sampler;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Jdbc采样器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/9 16:54
 */
@Setter
@Getter
@Document("wp_element")
public class JdbcRequestElement extends BaseElement {

	@DataField
	private String dataSource;
	@DataField
	private String queryType;
	@DataField
	private String query;
	@DataField
	private String queryArguments;
	@DataField
	private String queryArgumentsTypes;
	@DataField
	private String variableNames;
	@DataField
	private String resultVariable;
	@DataField
	private String queryTimeout;
	@DataField
	private String resultSetMaxRows;
	@DataField
	private String resultSetHandler;
}
