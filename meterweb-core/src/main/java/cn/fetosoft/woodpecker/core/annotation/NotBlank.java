package cn.fetosoft.woodpecker.core.annotation;

import java.lang.annotation.*;

/**
 * 不能为空
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/15 10:59
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface NotBlank {

	/**
	 * 错误信息
	 * @return
	 */
	String message() default "";
}
