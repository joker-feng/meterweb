package cn.fetosoft.woodpecker.core.jmeter.element.config;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.config.HttpHeaderElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import cn.fetosoft.woodpecker.core.jmeter.extension.HttpHeaderConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.protocol.http.control.Header;
import org.apache.jmeter.protocol.http.control.HeaderManager;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建Http请求头
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 11:34
 */
@Component("httpHeaderImpl")
public class CreateHttpHeaderImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		HttpHeaderElement element = (HttpHeaderElement) be;
		HttpHeaderConfig headerConfig = new HttpHeaderConfig();
		HeaderManager headerManager = new HeaderManager();
		String[] names = element.getNames();
		String[] values = element.getValues();
		if(names!=null && values!=null && names.length>0 &&
				names.length==values.length){
			for(int i=0; i<names.length; i++){
				headerManager.add(new Header(names[i], values[i]));
			}
		}
		headerConfig.setHeaderManager(headerManager);
		return headerConfig;
	}
}
