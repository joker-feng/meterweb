package cn.fetosoft.woodpecker.core.data.base;

import org.springframework.data.mongodb.core.mapping.FieldType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 查询条件
 * @author guobingbing
 * @create 2020/12/1 18:41
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface Condition {

	/**
	 * 字段类型
	 * @return
	 */
	FieldType fieldType() default FieldType.STRING;

	/**
	 * 条件类型
	 * @return
	 */
	ConditionType conditionType() default ConditionType.EQ;

	/**
	 * 字段名称，默认为空将通过反射获取字段名称
	 * @return
	 */
	String fieldName() default "";
}
