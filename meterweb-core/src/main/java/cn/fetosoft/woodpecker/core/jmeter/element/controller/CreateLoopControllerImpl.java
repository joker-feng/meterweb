package cn.fetosoft.woodpecker.core.jmeter.element.controller;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.controller.LoopControllerElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.control.LoopController;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建循环控制器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/16 10:23
 */
@Component("loopControllerImpl")
public class CreateLoopControllerImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		LoopControllerElement element = (LoopControllerElement) be;
		LoopController controller = new LoopController();
		controller.setContinueForever(true);
		if(StringUtils.isNotBlank(element.getForever())){
			if(Boolean.parseBoolean(element.getForever())){
				controller.setLoops(-1);
			}else{
				controller.setLoops(element.getLoops());
			}
		}else {
			controller.setLoops(element.getLoops());
		}
		return controller;
	}
}
