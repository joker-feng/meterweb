package cn.fetosoft.woodpecker.core.data.entity.element.processor;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 调试后置处理程序
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/28 14:36
 */
@Setter
@Getter
@Document("wp_element")
public class DebugPostElement extends BaseElement {

	@DataField
	private String displayJMeterProperties;

	@DataField
	private String displayJMeterVariables;

	@DataField
	private String displaySamplerProperties;

	@DataField
	private String displaySystemProperties;
}
