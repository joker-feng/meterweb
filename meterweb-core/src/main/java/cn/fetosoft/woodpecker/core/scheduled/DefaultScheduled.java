package cn.fetosoft.woodpecker.core.scheduled;

import cn.fetosoft.commons.scheduled.AbstractScheduled;
import cn.fetosoft.commons.scheduled.TaskInfo;
import org.springframework.stereotype.Component;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/27 16:10
 */
@Component
public class DefaultScheduled extends AbstractScheduled {

	@Override
	protected boolean isClusterExec(TaskInfo taskInfo) {
		return true;
	}
}
