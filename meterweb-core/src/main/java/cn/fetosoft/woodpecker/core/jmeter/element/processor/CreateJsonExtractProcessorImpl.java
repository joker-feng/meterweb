package cn.fetosoft.woodpecker.core.jmeter.element.processor;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.processor.JsonExtractElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.jmeter.extractor.json.jsonpath.JSONPostProcessor;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建JSON处理器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 11:48
 */
@Component("jsonExtractImpl")
public class CreateJsonExtractProcessorImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		JsonExtractElement jsonElement = (JsonExtractElement) be;
		JSONPostProcessor processor = new JSONPostProcessor();
		processor.setProperty(TestElement.TEST_CLASS, JSONPostProcessor.class.getName());
		processor.setRefNames(jsonElement.getReferenceNames());
		processor.setJsonPathExpressions(jsonElement.getJsonPathExprs());
		processor.setMatchNumbers(jsonElement.getMatchNumbers());
		processor.setDefaultValues(jsonElement.getDefaultValues());
		return processor;
	}
}
