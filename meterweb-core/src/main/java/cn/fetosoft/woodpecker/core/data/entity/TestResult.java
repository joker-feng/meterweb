package cn.fetosoft.woodpecker.core.data.entity;

import cn.fetosoft.woodpecker.core.enums.DateFormatEnum;
import cn.fetosoft.woodpecker.core.util.NumberFormatUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/24 11:12
 */
@Setter
@Getter
@Document("wp_result")
public class TestResult extends BaseElement {

	private String threadId;

	private String elementId;

	private String responseCode = "";

	private String samplerData;

	private String threadName = "";

	private String responseMessage = "";

	private String responseHeaders = "";

	private String requestHeaders = "";

	private String responseData = "";

	private int errorCount = 0;

	private long timeStamp = 0;

	private long startTime = 0;

	private long endTime = 0;

	private long idleTime = 0;

	private long elapsedTime = 0;

	private long latency = 0;

	private long connectTime = 0;

	private boolean stopThread;

	private boolean stopTest;

	private boolean stopTestNow;

	private int sampleCount = 1;

	private long bytes = 0;

	private int headersSize = 0;

	private long bodySize = 0;

	private int groupThreads = 0;

	private int allThreads = 0;

	private long sentBytes;

	private String dataType;

	private String contentType;

	private String startTimeFmt = "";

	private String endTimeFmt = "";

	private boolean assertionError = false;
	private boolean assertionFailure = false;
	private String assertionMessage = "";

	private String createTimeFmt;
	private String sendBytesFmt;

	public String getCreateTimeFmt() {
		if(this.getCreateTime()!=null){
			createTimeFmt = DateFormatEnum.YMD_HMS_SSS.timestampToString(this.getCreateTime());
		}
		return createTimeFmt;
	}

	public String getSendBytesFmt() {
		if(sentBytes>0){
			sendBytesFmt = NumberFormatUtil.doubleFormat(sentBytes, 4);
		}
		return sendBytesFmt;
	}
}
