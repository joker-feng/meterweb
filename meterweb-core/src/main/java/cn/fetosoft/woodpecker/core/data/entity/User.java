package cn.fetosoft.woodpecker.core.data.entity;

import cn.fetosoft.woodpecker.core.annotation.NotBlank;
import cn.fetosoft.woodpecker.core.data.base.BaseEntity;
import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.enums.DateFormatEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 用户
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/14 20:17
 */
@Setter
@Getter
@Document("wp_user")
public class User extends BaseEntity {

    @NotBlank(message = "用户ID不能为空")
    @DataField(unique = true, updatable = false)
    private String userId;

    @NotBlank(message = "用户名不能为空")
    @DataField(unique = true, updatable = false)
    private String username;

    @NotBlank(message = "密码不能为空")
    @DataField
    private String password;

	@NotBlank(message = "姓名不能为空")
    @DataField
    private String name;

    @DataField
    private String mail;

    @DataField
    private String avatar;

    @DataField
    private String loginIp;

    @DataField
    private Long loginTime;

    @DataField
    private String note;

    @DataField
    private String roleId;

    private String createTimeFmt;
    private String modifyTimeFmt;
    private String loginTimeFmt;

    public String getCreateTimeFmt() {
        if(this.getCreateTime()!=null){
            createTimeFmt = DateFormatEnum.YMD_HMS.timestampToString(this.getCreateTime());
        }
        return createTimeFmt;
    }

    public String getModifyTimeFmt() {
        if(this.getModifyTime()!=null){
            modifyTimeFmt = DateFormatEnum.YMD_HMS.timestampToString(this.getModifyTime());
        }
        return modifyTimeFmt;
    }

    public String getLoginTimeFmt() {
        if(this.getLoginTime()!=null){
            loginTimeFmt = DateFormatEnum.YMD_HMS.timestampToString(this.getLoginTime());
        }
        return loginTimeFmt;
    }
}
