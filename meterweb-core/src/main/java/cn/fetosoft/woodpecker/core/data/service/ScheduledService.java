package cn.fetosoft.woodpecker.core.data.service;

import cn.fetosoft.woodpecker.core.data.base.BaseDataService;
import cn.fetosoft.woodpecker.core.data.entity.Scheduled;
import cn.fetosoft.woodpecker.core.data.form.ScheduledForm;

import java.util.List;

/**
 * 定时服务
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/27 16:19
 */
public interface ScheduledService extends BaseDataService<Scheduled, ScheduledForm> {

	List<Scheduled> selectListByForm(ScheduledForm form);
}
