package cn.fetosoft.woodpecker.core.data.entity.element.controller;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 模块控制器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/26 11:27
 */
@Setter
@Getter
@Document("wp_element")
public class ModuleControllerElement extends BaseElement {

	@DataField
	private String fragments;
}
