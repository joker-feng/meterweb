package org.apache.jmeter;

import org.apache.jmeter.engine.JMeterEngine;
import org.apache.jmeter.engine.JMeterEngineException;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.services.FileServer;
import org.apache.jorphan.collections.HashTree;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/5/29 17:21
 */
public class JMeterStartTest {

	public static void startEngine() throws IOException {
		JMeterEngine engine = new StandardJMeterEngine();
		File f = new File("D:\\接口\\线程组.jmx");
		FileServer.getFileServer().setBaseForScript(f);
		HashTree tree = SaveService.loadTree(f);
		HashTree clonedTree = new HashTree();
		clonedTree.add(clonedTree.getArray()[0], new JMeter.ListenToTest(
				org.apache.jmeter.JMeter.ListenToTest.RunMode.LOCAL, false, null));
		engine.configure(clonedTree);
		long now=System.currentTimeMillis();
		System.out.println("Starting standalone test @ "+new Date(now)+" ("+now+")");
		try {
			engine.runTest();
		} catch (JMeterEngineException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
//		JMeter jMeter = new JMeter();
//		jMeter.start(args);
		startEngine();
	}
}
